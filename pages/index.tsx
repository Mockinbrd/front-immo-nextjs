import { GetStaticProps } from "next";
import Head from "next/head";
import Layout, { siteTitle } from "../components/layout";
import utilStyles from "../styles/utils.module.css";

type FooData = {
  id: number;
  name: string;
};

export const getStaticProps: GetStaticProps = async () => {
  const fooData = [
    {
      id: 1,
      name: "James",
    },
    {
      id: 2,
      name: "Dwayne",
    },
  ];
  return {
    props: {
      fooData,
    },
  };
};

export default function Home({ fooData }: { fooData: FooData[] }): JSX.Element {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p className="font-bold">[Your Self Introduction]</p>
        <p>
          (This is a sample website - you’ll be building a site like this on{" "}
          <a href="https://nextjs.org/learn">our Next.js tutorial</a>.)
        </p>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        <ul className={utilStyles.list}>
          {fooData.map(({ id, name }) => (
            <li className={utilStyles.listItem} key={id}>
              {name}
              <br />
              {id}
            </li>
          ))}
        </ul>
      </section>
    </Layout>
  );
}
